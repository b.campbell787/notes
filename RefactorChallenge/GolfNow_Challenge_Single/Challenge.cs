/*
 * GolfNow engineer challenge instructions.
 *
 * Please refactor 'Player', 'Member', 'Visitor' and 'BookingCreator' to make them more
 * efficient, maintainable, reusable and testable. You may also update the 'example usage'
 * code to suit your changes.
 *
 * Do not change 'IBookingStore', 'ILogger', 'MySqlBookingStore' or 'FileLogger'.
 *
 * You should not need to add any new comments - ideally your code should be self-documenting!
 *
 * Do not be concerned with the number of lines of code in your solution - smaller is not
 * necessarily better!
 *
 * Hint 1: Program to interface, not implementation
 * Hint 2: We may need different booking storage and logging mechanisms in the future
 * Hint 3: BookingCreator should not be concerned with the type of player making a booking
 */

using System;

interface IBookingStore
{
    void AddBooking(string name, float price);
}

interface ILogger
{
    void AddEntry(string text);
}

class MySqlBookingStore : IBookingStore
{
    public void AddBooking(string name, float price)
    {
        // Note: actual MySQL storage routine is irrelevant for this challenge.
        Console.WriteLine("Storing booking for '{0}' in MySQL database with price {1:C2}", name, price);
    }
}

class FileLogger : ILogger
{
    public FileLogger(string filename)
    {
        Filename = filename;
    }

    public string Filename { get; set; }

    public void AddEntry(string text)
    {
        // Note: actual file logging routine is irrelevant for this challenge.
        Console.WriteLine("'{0}' >> {1}", text, Filename);
    }
}

// Anything under this line can be changed!

interface IPlayer
{
    string Name { get; set; }
    int Cost { get; set; }

}

abstract class Player : IPlayer
{
    public string Name { get; set; }
    public virtual int Cost { get; set; }
}

class Member : Player
{
    public override int Cost { get => 10; }

}

class Visitor : Player
{
    public override int Cost { get => 30; }

}

class BookingCreator
{
    PlayerChecker playerChecker = new PlayerChecker();

    public void CreatePlayerBooking<T>(T player, string playerName, int playerCost)
    {

        MySqlBookingStore bookingStore = new MySqlBookingStore();

        if (playerChecker.PlayerTypeResponse(player, playerName))
        {
            bookingStore.AddBooking(playerName, playerCost);
        }

    }

}

class PlayerChecker
{
    FileLogger fileLogger;

    public bool PlayerTypeResponse<T>(T playerType, string playerName)
    {
        string fileLocation = "/var/log/bookings.log";

        fileLogger = new FileLogger(fileLocation);

        switch (playerType)
        {
            case Member _:
                fileLogger.AddEntry("A new member booking has been created for " + playerName);
                return true;
            case Visitor _:
                fileLogger.AddEntry("A new visitor booking has been created for " + playerName);
                return true;
            default:
                try
                {
                    fileLogger.AddEntry($"ERROR: Unknown player trying to make a booking... {playerName}, {playerType.GetType()}");
                }
                catch (NotSupportedException)
                {
                    throw new NotSupportedException();
                }

                return false;
        }

    }
}

class Challenge
{
    static void Main()
        {
            //Example usage...
            Member member = new Member();
            member.Name = "David";

            Visitor visitor = new Visitor();
            visitor.Name = "Andy";

            BookingCreator bookingCreator = new BookingCreator();
            bookingCreator.CreatePlayerBooking(member, member.Name, member.Cost);
            bookingCreator.CreatePlayerBooking(visitor, visitor.Name, visitor.Cost);


            //Testing
            TestChallenge testChallenge = new TestChallenge();
            testChallenge.Test_Incorrect_Player_Type();

            Console.ReadLine();
        }
}

class TestChallenge
{

    public void Test_Incorrect_Player_Type()
    {
        TestPlayer invalidPlayer = new TestPlayer();
        BookingCreator bookingCreator = new BookingCreator();

        invalidPlayer.Name = "Brendan";
        invalidPlayer.Cost = 99;

        bookingCreator.CreatePlayerBooking(invalidPlayer, invalidPlayer.Name, invalidPlayer.Cost);
    }
}

class TestPlayer
{
    public string Name { get; set; }
    public int Cost { get; set; }
}
