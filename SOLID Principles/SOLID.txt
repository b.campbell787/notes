SOLID

Code should be written in the simplest way intially
Then were there are areas that are difficult, look for optimisation 
Research whether SOLID princicples can apply here 


SRP - Single Responsibility Principle
    - Each software module should have one and only one reason to change
    - ie. a class or method or function

    Aim here is to separate the 'what' from the 'how' and this is done via delegation and encapsulation
    Doing a singluar task in a particular way; other classes can delegate a certain task to an instance of a class that encapsulates performing that task

    Responsibility: 
        - Decision our code is making about specific implementation details of a part of what the application does
        - The 'how' something is done 
        - Responsibilities:
            > Persistence 
            > Logging
            > Validation
            > Business logic
        - All code may change at any time; 
            > CIO may require change to persistence due to vendor change (local hosting to AWS)
            > CSO may require more robust logging framework that will now track security breaches
            > COO ensure that a recent acquisition now allows 'x' users to use the system and be validated onto it 
            > CMO annouced a regulator source change (Brexit) so business rules must be updated 


    Key Terms
        Coupling: 
            - TIGHT:    Binds two or more details are inter-mixed within the same class, it can be difficult to change either of those details
            - LOOSE:    An approach that allows different details of an aplpication to interact with one another in modular fashion
        
        Separation of Concerns:
            - Programs should be separated into distinct sections that each address a separate concern or set of information that affects the program 
            - ie. you don't keep your cleaning products in the fridge
            - Ensures that high level business logic avoids having to deal with low level implementation code 

        Cohesion:
            - How closely related elements of a class are to one another.
            - Class with many responsibilities has low cohesion 
            - If a class has several methods that do not interact with one another, should they be moved into another class? 

    






OCP - Open Closed Principle

LSP - Liskov Substitution Principle

ISP - Interface Segregation Principle

DIP - Dependency Inversion Principle 

 